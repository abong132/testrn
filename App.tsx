import AppProvider from 'abong.code/context/AppProvider';
import AppNavigation from './app/navigation/AppNavigation';
import React from 'react';
import 'react-native-gesture-handler';

const App = () => {
  return (
    <AppProvider>
      <AppNavigation />
    </AppProvider>
  );
};

export default App;
