import React, {
  Dispatch,
  ReactNode,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {PaperProvider} from 'react-native-paper';
import Toast, {
  BaseToast,
  ErrorToast,
  SuccessToast,
} from 'react-native-toast-message';
import color from 'abong.code/theme/color';

export type UserType = {
  id: string;
  name: string;
  email: string;
  avatar: string;
};

type AppContextType = {
  user: UserType;
  setUser: Dispatch<SetStateAction<UserType>>;
};
const AppContext = createContext({} as AppContextType);
export const useAppContext = () => useContext(AppContext);
const AppProvider = ({children}: {children: ReactNode}) => {
  const [user, setUser] = useState<UserType>({} as UserType);
  return (
    <AppContext.Provider value={{user, setUser}}>
      <SafeAreaProvider>
        <PaperProvider>{children}</PaperProvider>
        <Toast config={toastConfig} />
      </SafeAreaProvider>
    </AppContext.Provider>
  );
};

export default AppProvider;

const toastConfig = {
  // <Toast> must at the end line
  success: (props: any) => (
    <SuccessToast
      {...props}
      text1Style={props.props.text1Style}
      text2Style={props.props.text2Style}
      text2NumberOfLines={2}
    />
  ),
  error: (props: any) => (
    <ErrorToast
      {...props}
      text1Style={props.props.text1Style}
      text2Style={props.props.text2Style}
      text2NumberOfLines={2}
    />
  ),
  info: (props: any) => (
    <BaseToast
      {...props}
      style={{borderLeftColor: color.warning}}
      text1Style={props.props.text1Style}
      text2Style={props.props.text2Style}
      text2NumberOfLines={2}
    />
  ),
};
