// /**
//  * @format
//  */

import {getProduct} from 'app/api/product';
import axios from 'axios';

// import 'react-native';
// import React from 'react';
// import App from '../App';

// // Note: import explicitly to use the types shiped with jest.
// import {it} from '@jest/globals';

// // Note: test renderer must be required after react-native.
// import renderer from 'react-test-renderer';

// it('renders correctly', () => {
//   renderer.create(<App />);
// });

describe('getProduct', () => {
  // Returns an array of products when API call is successful
  it('should return an array of products when API call is successful', async () => {
    const mockResponse = {
      status: 200,
      data: [
        {
          id: '1',
          product: 'Product 1',
          product_description: 'Description 1',
          product_price: '10',
          created_at: '2022-01-01',
        },
        {
          id: '2',
          product: 'Product 2',
          product_description: 'Description 2',
          product_price: '20',
          created_at: '2022-01-02',
        },
      ],
    };
    jest.spyOn(axios, 'get').mockResolvedValue(mockResponse);

    const result = await getProduct();

    expect(result).toEqual(mockResponse.data);
    expect(axios.get).toHaveBeenCalledWith(
      'https://64c9c51cb2980cec85c26650.mockapi.io/products',
    );
  });

  // Returns an empty array when API call returns no products
  it('should return an empty array when API call returns no products', async () => {
    const mockResponse = {
      status: 200,
      data: [],
    };
    jest.spyOn(axios, 'get').mockResolvedValue(mockResponse);

    const result = await getProduct();

    expect(result).toEqual(mockResponse.data);
    expect(axios.get).toHaveBeenCalledWith(
      'https://64c9c51cb2980cec85c26650.mockapi.io/products',
    );
  });

  // Throws an error when API call fails
  it('should throw an error when API call fails', async () => {
    const mockResponse = {
      status: 500,
    };
    jest.spyOn(axios, 'get').mockResolvedValue(mockResponse);

    await expect(getProduct()).rejects.toThrow(
      `Request failed with status ${mockResponse.status}`,
    );
    expect(axios.get).toHaveBeenCalledWith(
      'https://64c9c51cb2980cec85c26650.mockapi.io/products',
    );
  });

  // Handles large response data from API
  it('should handle large response data from API', async () => {
    const mockResponse = {
      status: 200,
      data: Array.from({length: 10000}, (_, index) => ({
        id: String(index),
        product: `Product ${index}`,
        product_description: `Description ${index}`,
        product_price: String(index),
        created_at: '2022-01-01',
      })),
    };
    jest.spyOn(axios, 'get').mockResolvedValue(mockResponse);

    const result = await getProduct();

    expect(result).toEqual(mockResponse.data);
    expect(axios.get).toHaveBeenCalledWith(
      'https://64c9c51cb2980cec85c26650.mockapi.io/products',
    );
  });
});
