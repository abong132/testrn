import dayjs from 'dayjs';

export const convertDateFormat = (inputDate: string) => {
  if (!dayjs(inputDate).isValid()) {
    throw new Error('Invalid input date');
  }
  const date = dayjs(inputDate);
  const formattedDate = date.format('HH:mm - DD/MM/YYYY');
  return formattedDate;
};
