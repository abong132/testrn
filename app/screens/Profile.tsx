import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import color from 'abong.code/theme/color';
import {appSize} from 'abong.code/config/AppConstant';
import {UserType, useAppContext} from 'abong.code/context/AppProvider';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootAppNavigationParams} from 'app/navigation/parms';

const Profile = () => {
  const {setUser} = useAppContext();
  const navigation =
    useNavigation<StackNavigationProp<RootAppNavigationParams>>();
  const handleLogOut = () => {
    setUser({} as UserType);
    navigation.pop();

    navigation.replace('Login');
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.btn} onPress={handleLogOut}>
        <Text style={{color: color.black}}>Log Out</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    borderWidth: appSize(1),
    padding: appSize(10),
    borderRadius: appSize(10),
  },
});
