import {View, StyleSheet, FlatList} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import Header from 'app/screens/Home/container/Header';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import color from 'abong.code/theme/color';
import {Product, getProduct} from 'app/api/product';
import ItemProduct from 'app/components/ItemProduct';
import AppConstant, {appSize} from 'abong.code/config/AppConstant';
import {ActivityIndicator} from 'react-native-paper';
import {showToastMessageError} from 'abong.code/helpers/messageHelper';

const Home = () => {
  const {top, bottom} = useSafeAreaInsets();
  const scrollBegin = useRef(false);

  const [page, setPage] = useState(1);
  const [refreshing, setRefreshing] = useState(false);
  const [noLoadMore, setNoLoadMore] = useState(false);
  const [dataProducts, setDataProducts] = useState<Product[]>([]);
  const loadMore = () => {
    if (scrollBegin.current) {
      setTimeout(() => {
        setPage(page + 1);
      }, 2000);
    }
    scrollBegin.current = false;
  };

  const renderItem = ({item}: {item: Product}) => {
    return <ItemProduct {...item} />;
  };
  useEffect(() => {
    if (
      dataProducts.length > 0 &&
      page * AppConstant.LIST_SIZE >= dataProducts.length
    ) {
      setNoLoadMore(true);
    }
  }, [dataProducts.length, page]);

  useEffect(() => {
    getProduct()
      .then(res => setDataProducts(res))
      .catch(err => showToastMessageError('Error!', err.toString()));
  }, []);

  return (
    <View style={[styles.container, {paddingTop: top, paddingBottom: bottom}]}>
      <Header />
      <FlatList
        refreshing={refreshing}
        onRefresh={() => {
          setRefreshing(true);
          setTimeout(() => {
            setPage(1);
            setRefreshing(false);
          }, 1500);
        }}
        data={dataProducts.slice(0, page * AppConstant.LIST_SIZE)}
        keyExtractor={item => item.id.toString()}
        renderItem={renderItem}
        onEndReached={loadMore}
        onEndReachedThreshold={0.1}
        showsVerticalScrollIndicator={false}
        ListFooterComponent={
          noLoadMore ? null : <ActivityIndicator color={color.primary} />
        }
        onMomentumScrollBegin={() => {
          scrollBegin.current = true;
        }}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
    paddingHorizontal: appSize(16),
  },
});
