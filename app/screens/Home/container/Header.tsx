import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {AvatarUser, Logo} from 'app/images';
import {appSize} from 'abong.code/config/AppConstant';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootAppNavigationParams} from 'app/navigation/parms';

const Header = () => {
  const navigation =
    useNavigation<StackNavigationProp<RootAppNavigationParams>>();
  return (
    <View style={styles.container}>
      <Image source={Logo} resizeMode="contain" style={styles.logo} />
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Profile');
        }}>
        <Image source={AvatarUser} resizeMode="contain" style={styles.avatar} />
      </TouchableOpacity>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    height: appSize(50),
    paddingHorizontal: appSize(16),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  logo: {
    width: appSize(150),
    height: appSize(50),
  },
  avatar: {
    width: appSize(35),
    height: appSize(35),
    borderRadius: appSize(35),
  },
});
