import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import color from 'abong.code/theme/color';
import {appSize} from 'abong.code/config/AppConstant';
import {TextInput} from 'react-native-paper';
import {isEmail} from 'class-validator';
import {showToastMessageSuccess} from 'abong.code/helpers/messageHelper';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootAppNavigationParams} from 'app/navigation/parms';
import {Logo} from 'app/images';
import {useAppContext} from 'abong.code/context/AppProvider';

const Login = () => {
  const {setUser} = useAppContext();
  const navigation =
    useNavigation<StackNavigationProp<RootAppNavigationParams>>();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [validateEmail, setValidateEmail] = useState(false);
  return (
    <View style={styles.container}>
      <Image source={Logo} resizeMode="contain" style={styles.logo} />
      <TextInput
        mode="outlined"
        label="Name"
        value={name}
        onChangeText={setName}
        style={styles.textInput}
      />
      <TextInput
        autoCapitalize="none"
        mode="outlined"
        label="Email"
        value={email}
        onChangeText={setEmail}
        style={styles.textInput}
        onContentSizeChange={() => {
          setValidateEmail(isEmail(email));
        }}
      />
      {email && !validateEmail && (
        <View style={styles.fullWidth}>
          <Text style={styles.textError}>Email không hợp lệ.</Text>
        </View>
      )}
      <TouchableOpacity
        disabled={!validateEmail || !name}
        style={[
          styles.button,
          {
            backgroundColor:
              !name || !validateEmail ? color.disabled : color.primary,
          },
        ]}
        onPress={() => {
          setUser({
            id: Math.floor(Math.random() * 1000).toString(),
            email,
            name,
            avatar: '',
          });
          showToastMessageSuccess('', 'Login success!');

          navigation.navigate('Home');
        }}>
        <Text style={styles.textButton}>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
    alignItems: 'center',
    paddingHorizontal: appSize(16),
    justifyContent: 'center',
  },
  logo: {
    width: appSize(280),
    height: appSize(100),
    marginBottom: appSize(50),
  },
  textInput: {
    width: '100%',
    marginTop: appSize(15),
  },
  button: {
    width: '70%',
    alignItems: 'center',
    justifyContent: 'center',
    height: appSize(45),
    borderRadius: appSize(30),
    marginTop: appSize(50),
  },
  textButton: {
    color: color.white,
    fontSize: appSize(16),
    fontWeight: '700',
  },
  fullWidth: {
    width: '100%',
  },
  textError: {
    color: color.danger,
  },
});
