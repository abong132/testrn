import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {RouteProp, useRoute} from '@react-navigation/native';
import {RootAppNavigationParams} from 'app/navigation/parms';
import color from 'abong.code/theme/color';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Header from 'app/components/Header';
import {appSize} from 'abong.code/config/AppConstant';
import {convertDateFormat} from 'app/utils/string';
import {showToastMessageSuccess} from 'abong.code/helpers/messageHelper';
import CartIcon from 'assets/icons/CartIcon';
import CommentForm from 'app/components/CommentForm';
import Comment from 'app/components/Comment';
import {useAppContext} from 'abong.code/context/AppProvider';

export type CommentType = {
  id: string;
  text: string;
  created_at: string;
  author: {
    id: string;
    name: string;
    avatar: string;
  };
  comments: Omit<CommentType, 'comments'>[];
};
const comments: CommentType[] = [
  {
    id: '1',
    text: 'comment 1',
    created_at: new Date().toString(),
    author: {
      id: '1',
      name: 'Hoàng',
      avatar:
        'https://res.cloudinary.com/dplbq3fbl/image/upload/v1693064642/chat_app/odwnsuj6gec89lmnogv4.jpg',
    },
    comments: [
      {
        id: '2',
        text: 'comment 11 vnv mgmv ,v,v mgmagm ., fafmklf .ầdfd .gakfmvmb/. ầ ga g.g.ag.a.ga.g. bgvgbababmmvmv',
        created_at: new Date().toString(),
        author: {
          id: '2',
          name: 'Nga',
          avatar:
            'https://res.cloudinary.com/dplbq3fbl/image/upload/v1693215523/chat_app/lcy4cnjwdcnmlvtqggrk.jpg',
        },
      },
      {
        id: '3',
        text: 'comment 22',
        created_at: new Date().toString(),
        author: {
          id: '3',
          name: 'Long',
          avatar: '',
        },
      },
    ],
  },
  {
    id: '4',
    text: 'comment 2',
    created_at: new Date().toString(),
    author: {
      id: '4',
      name: 'Sơn',
      avatar: '',
    },
    comments: [
      {
        id: '5',
        text: 'comment 11',
        created_at: new Date().toString(),
        author: {
          id: '2',
          name: 'Nga',
          avatar:
            'https://res.cloudinary.com/dplbq3fbl/image/upload/v1693215523/chat_app/lcy4cnjwdcnmlvtqggrk.jpg',
        },
      },
    ],
  },
];
const DetailProduct = () => {
  const {top, bottom} = useSafeAreaInsets();
  const {
    params: {product},
  } = useRoute<RouteProp<RootAppNavigationParams, 'DetailProduct'>>();
  const {user} = useAppContext();

  const flatlistRef = useRef<any>();

  const [valueComment, setValueComment] = useState('');
  const [commentsArray, setCommentsArray] = useState<CommentType[]>([]);

  const handleComment = () => {
    const cloneComments = [...commentsArray];
    cloneComments.push({
      id: Math.floor(Math.random() * 1000).toString(),
      text: valueComment,
      created_at: new Date().toString(),
      author: user,
      comments: [],
    });
    setValueComment('');
    setCommentsArray(cloneComments);
    setTimeout(() => {
      flatlistRef.current.scrollToEnd({animated: true});
    }, 500);
  };
  useEffect(() => {
    setCommentsArray(comments);
  }, []);
  return (
    <View style={[styles.container, {paddingTop: top, paddingBottom: bottom}]}>
      <Header title="Detail" />
      <View style={styles.content}>
        <Text style={styles.nameProduct}>{product.product}</Text>
        <Text style={styles.time}>{convertDateFormat(product.created_at)}</Text>
        <View style={[styles.row, {marginTop: appSize(10)}]}>
          <Text style={styles.titlePrice}>Price: </Text>
          <Text style={styles.price}>{product.product_price}$</Text>
        </View>
        <ScrollView style={styles.contentDesc}>
          <Text style={styles.desc}>{product.product_description}</Text>
        </ScrollView>
        <TouchableOpacity
          style={[styles.button, styles.row]}
          onPress={() => {
            showToastMessageSuccess('', 'Product added to cart!');
          }}>
          <CartIcon color={color.white} />
          <Text style={styles.textButton}>Buy</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.contentComment}>
        <Comment
          comments={commentsArray}
          setComments={setCommentsArray}
          flatlistRef={flatlistRef}
        />
        <CommentForm
          value={valueComment}
          onChangeText={setValueComment}
          handleComment={handleComment}
        />
      </View>
    </View>
  );
};

export default DetailProduct;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
    paddingHorizontal: appSize(16),
  },
  content: {
    alignItems: 'center',
  },
  nameProduct: {
    fontSize: appSize(22),
    fontWeight: 'bold',
    marginTop: appSize(30),
    color: color.black,
  },
  time: {
    fontSize: appSize(12),
    marginTop: appSize(5),
    color: color.black04,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titlePrice: {
    fontStyle: 'italic',
    color: color.black,
  },
  price: {
    color: color.red,
  },
  desc: {
    color: color.black,
    textAlign: 'justify',
  },
  button: {
    width: '70%',
    alignItems: 'center',
    justifyContent: 'center',
    height: appSize(40),
    borderRadius: appSize(30),
    marginTop: appSize(40),
    backgroundColor: color.primary,
  },
  textButton: {
    color: color.white,
    fontSize: appSize(16),
    marginLeft: appSize(5),
  },
  contentDesc: {
    marginTop: appSize(20),
    maxHeight: appSize(200),
  },
  contentComment: {
    flex: 1,
  },
});
