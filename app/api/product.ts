import axios from 'axios';

export type Product = {
  id: string;
  product: string;
  product_description: string;
  product_price: string;
  created_at: string;
};
// export const getProduct = async (): Promise<Product[]> =>
//   await axios
//     .get('https://64c9c51cb2980cec85c26650.mockapi.io/products')
//     .then(({data}) => data)
//     .catch(err => {
//       throw new Error(err);
//     });

export const getProduct = async (): Promise<Product[]> => {
  const response = await axios.get(
    'https://64c9c51cb2980cec85c26650.mockapi.io/products',
  );
  if (response.status !== 200) {
    throw new Error(`Request failed with status ${response.status}`);
  }
  return response.data;
};
