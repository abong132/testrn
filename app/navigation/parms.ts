import {Product} from 'app/api/product';

export type RootAppNavigationParams = {
  Login: undefined;
  Home: undefined;
  DetailProduct: {product: Product};
  Profile: undefined;
};
