import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {Dispatch, SetStateAction} from 'react';
import {CommentType} from 'app/screens/DetailProduct';
import ItemComment from './ItemComment';
import color from 'abong.code/theme/color';
import {appSize} from 'abong.code/config/AppConstant';

const Comment = ({
  comments,
  setComments,
  flatlistRef,
}: {
  comments: CommentType[];
  setComments: Dispatch<SetStateAction<CommentType[]>>;
  flatlistRef: any;
}) => {
  const renderItem = ({item}: {item: CommentType}) => {
    return (
      <ItemComment
        comment={item}
        commentsData={comments}
        setCommentsData={setComments}
      />
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Comments</Text>
      <FlatList
        ref={flatlistRef}
        data={comments}
        keyExtractor={(_, index) => index.toString()}
        renderItem={renderItem}
      />
    </View>
  );
};

export default Comment;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: appSize(10),
  },
  title: {
    color: color.black,
    fontSize: appSize(16),
    marginBottom: appSize(8),
    fontWeight: '500',
  },
});
