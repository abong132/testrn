import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import BackIcon from 'assets/icons/BackIcon';
import color from 'abong.code/theme/color';
import {appSize} from 'abong.code/config/AppConstant';
import {useNavigation} from '@react-navigation/native';

type Props = {
  title: string;
};
const Header = ({title}: Props) => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}>
        <BackIcon color={color.black} />
      </TouchableOpacity>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    position: 'absolute',
    textAlign: 'center',
    right: 0,
    left: 0,
    fontSize: appSize(18),
    fontWeight: '700',
    color: color.black,
  },
});
