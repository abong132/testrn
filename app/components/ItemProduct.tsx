import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import {appSize} from 'abong.code/config/AppConstant';
import {Product} from 'app/api/product';
import color from 'abong.code/theme/color';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootAppNavigationParams} from 'app/navigation/parms';
import {convertDateFormat} from 'app/utils/string';

const ItemProduct = (props: Product) => {
  const navigation =
    useNavigation<StackNavigationProp<RootAppNavigationParams>>();
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        navigation.navigate('DetailProduct', {product: props});
      }}>
      <View style={styles.content}>
        <Text style={styles.nameProduct}>{props.product}</Text>
        <Text style={styles.time}>{convertDateFormat(props.created_at)}</Text>
      </View>
      <Text numberOfLines={2} style={styles.desc}>
        {props.product_description}
      </Text>
      <View style={styles.row}>
        <Text style={styles.titlePrice}>Price:</Text>
        <Text style={styles.price}>{props.product_price}$</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ItemProduct;

const styles = StyleSheet.create({
  container: {
    marginBottom: appSize(8),
    padding: appSize(10),
    shadowColor: color.primary,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2.5,

    elevation: 5,
    backgroundColor: color.white,
    borderRadius: appSize(10),
    marginHorizontal: appSize(1),
  },
  nameProduct: {
    color: color.black,
    fontSize: appSize(18),
    fontWeight: '700',
    width: appSize(200),
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  time: {
    fontSize: appSize(12),
    marginTop: appSize(5),
    color: color.black04,
  },
  desc: {
    color: color.textBtnLike,
    marginVertical: appSize(5),
    textAlign: 'justify',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  price: {
    color: color.red,
    marginLeft: appSize(10),
  },
  titlePrice: {
    color: color.black,
    fontStyle: 'italic',
  },
});
