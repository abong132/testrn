import {StyleSheet, TextInput, TouchableOpacity, View} from 'react-native';
import React, {Dispatch, SetStateAction} from 'react';
import color from 'abong.code/theme/color';
import {appSize} from 'abong.code/config/AppConstant';
import SendIcon from 'assets/icons/SendIcon';

type Props = {
  value: string;
  onChangeText: Dispatch<SetStateAction<string>>;
  handleComment: () => void;
};
const CommentForm = (props: Props) => {
  return (
    <View style={styles.container}>
      <TextInput
        placeholder="write a comment..."
        placeholderTextColor={color.placeholder}
        value={props.value}
        onChangeText={props.onChangeText}
        style={styles.textInput}
      />
      <TouchableOpacity disabled={!props.value} onPress={props.handleComment}>
        <SendIcon color={props.value ? color.primary : color.placeholder} />
      </TouchableOpacity>
    </View>
  );
};

export default CommentForm;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: appSize(5),
  },
  textInput: {
    width: '100%',
    borderRadius: appSize(10),
    borderWidth: appSize(1),
    flex: 1,
    padding: appSize(10),
    borderColor: color.black04,
    marginRight: appSize(10),
  },
});
