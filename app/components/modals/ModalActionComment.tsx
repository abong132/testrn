import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Modal from 'react-native-modal';
import {appSize} from 'abong.code/config/AppConstant';
import color from 'abong.code/theme/color';
import {ItemCommentProps} from '../ItemComment';
interface Props extends ItemCommentProps {
  visible: boolean;
  onClose: () => void;
}

const ModalActionComment = (props: Props) => {
  const [isEdit, setIsEdit] = useState(false);
  const [valueEdit, setValueEdit] = useState('');

  const handleRemove = () => {
    if (props.commentsData.some(e => e.id === props.comment.id)) {
      const listAfterRemove = props.commentsData.filter(
        cmt => cmt.id !== props.comment.id,
      );
      props.setCommentsData(listAfterRemove);
    }
    if (
      props.commentsData.some(e =>
        e.comments.some(eSub => eSub.id === props.comment.id),
      )
    ) {
      const cloneComments = [...props.commentsData];
      const newArrayComments = cloneComments.map(cmt => {
        if (cmt.comments.some(e => e.id === props.comment.id)) {
          const cmtSub = cmt.comments.filter(e => e.id !== props.comment.id);
          return {
            ...cmt,
            comments: cmtSub,
          };
        }
        return cmt;
      });
      props.setCommentsData(newArrayComments);
    }
    setIsEdit(false);
    props.onClose();
  };

  const handleEdit = () => {
    if (props.commentsData.some(e => e.id === props.comment.id)) {
      const listAfter = props.commentsData.map(cmt => {
        if (cmt.id === props.comment.id) {
          return {
            ...cmt,
            text: valueEdit,
          };
        }
        return cmt;
      });
      props.setCommentsData(listAfter);
    }
    if (
      props.commentsData.some(e =>
        e.comments.some(eSub => eSub.id === props.comment.id),
      )
    ) {
      const newArrayComments = props.commentsData.map(cmt => {
        if (cmt.comments.some(e => e.id === props.comment.id)) {
          const cmtSub = cmt.comments.map(e => {
            if (e.id === props.comment.id) {
              return {
                ...e,
                text: valueEdit,
              };
            }
            return e;
          });
          return {
            ...cmt,
            comments: cmtSub,
          };
        }
        return cmt;
      });
      props.setCommentsData(newArrayComments);
    }
    setIsEdit(false);
    props.onClose();
  };
  useEffect(() => {
    setValueEdit(props.comment.text);
  }, [props.comment.text]);
  return (
    <Modal
      animationIn={'slideInUp'}
      animationOut={'slideOutDown'}
      isVisible={props.visible}
      onBackdropPress={props.onClose}
      style={styles.modal}>
      <View style={styles.content}>
        {isEdit ? (
          <>
            <Text style={styles.titleEdit}>Edit</Text>
            <TextInput
              value={valueEdit}
              onChangeText={setValueEdit}
              style={styles.textInput}
            />
            <View style={styles.contentBtnSub}>
              <TouchableOpacity
                style={styles.btnCancel}
                onPress={() => {
                  props.onClose();
                  setIsEdit(false);
                }}>
                <Text style={styles.textBtnSub}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.btnEdit} onPress={handleEdit}>
                <Text style={styles.textBtnSub}>Edit</Text>
              </TouchableOpacity>
            </View>
          </>
        ) : (
          <>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => setIsEdit(true)}>
              <Text>Edit</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={handleRemove}>
              <Text>Remove</Text>
            </TouchableOpacity>
          </>
        )}
      </View>
    </Modal>
  );
};

export default ModalActionComment;

const styles = StyleSheet.create({
  content: {
    width: '100%',
    height: appSize(300),
    backgroundColor: color.white,
    borderTopLeftRadius: appSize(20),
    borderTopRightRadius: appSize(20),
    padding: appSize(16),
  },
  modal: {
    margin: 0,
    justifyContent: 'flex-end',
  },
  btn: {
    borderBottomWidth: appSize(0.5),
    paddingVertical: appSize(10),
  },
  textInput: {
    borderWidth: appSize(0.5),
    padding: appSize(10),
    borderRadius: appSize(10),
  },
  titleEdit: {
    color: color.black,
    alignSelf: 'center',
    marginBottom: appSize(10),
    fontWeight: 'bold',
  },
  contentBtnSub: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: appSize(10),
  },
  btnCancel: {
    backgroundColor: color.danger,
    paddingVertical: appSize(5),
    paddingHorizontal: appSize(10),
    borderRadius: appSize(5),
  },
  btnEdit: {
    borderRadius: appSize(5),
    backgroundColor: color.primary,
    paddingVertical: appSize(5),
    paddingHorizontal: appSize(10),
    marginLeft: appSize(10),
  },
  textBtnSub: {
    color: color.white,
    fontWeight: '500',
  },
});
