import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Dispatch, SetStateAction, useState} from 'react';
import {CommentType} from 'app/screens/DetailProduct';
import {AvatarUser} from 'app/images';
import {appSize} from 'abong.code/config/AppConstant';
import color from 'abong.code/theme/color';
import CommentForm from './CommentForm';
import {useAppContext} from 'abong.code/context/AppProvider';
import ModalActionComment from './modals/ModalActionComment';

export type ItemCommentProps = {
  comment: CommentType;
  commentsData: CommentType[];
  setCommentsData: Dispatch<SetStateAction<CommentType[]>>;
};
const ItemComment = (props: ItemCommentProps) => {
  const {user} = useAppContext();
  const [showCommentForm, setShowCommentForm] = useState(false);
  const [valueComment, setValueComment] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [commentSelect, setCommentSelect] = useState<CommentType>(
    {} as CommentType,
  );

  const handleComment = () => {
    const cloneComments = [...props.commentsData];
    const newArrayComments = cloneComments.map(cmt => {
      if (props.comment.id === cmt.id) {
        return {
          ...cmt,
          comments: [
            ...cmt.comments,
            {
              id: Math.floor(Math.random() * 1000).toString(),
              text: valueComment,
              created_at: new Date().toString(),
              author: user,
            },
          ],
        };
      }
      return cmt;
    });
    props.setCommentsData(newArrayComments);
    setValueComment('');
    setShowCommentForm(false);
  };
  console.log(commentSelect, 'e');
  return (
    <View>
      <View style={styles.container}>
        <Image
          source={
            props.comment.author.avatar
              ? {uri: props.comment.author.avatar}
              : AvatarUser
          }
          resizeMode="cover"
          style={styles.avatar}
        />
        <View style={styles.flex}>
          <View style={styles.row}>
            <Pressable
              style={styles.viewComment}
              onLongPress={() => {
                if (props.comment.author.id === user.id) {
                  setCommentSelect(props.comment);
                  setShowModal(true);
                }
              }}>
              <Text style={styles.nameAuthor}>{props.comment.author.name}</Text>
              <Text style={styles.textComment}>{props.comment.text}</Text>
            </Pressable>
          </View>
          <TouchableOpacity
            onPress={() => {
              setShowCommentForm(true);
            }}>
            <Text style={styles.textReply}>Reply</Text>
          </TouchableOpacity>
          {showCommentForm && (
            <CommentForm
              value={valueComment}
              onChangeText={setValueComment}
              handleComment={handleComment}
            />
          )}
        </View>
      </View>
      {props.comment.comments.length > 0 &&
        props.comment.comments.map(item => {
          return (
            <View
              key={item.id}
              style={[styles.container, {marginLeft: appSize(50)}]}>
              <Image
                source={
                  item.author.avatar ? {uri: item.author.avatar} : AvatarUser
                }
                resizeMode="cover"
                style={styles.avatarSub}
              />
              <View style={styles.flex}>
                <View style={styles.row}>
                  <Pressable
                    style={styles.viewComment}
                    onLongPress={() => {
                      if (item.author.id === user.id) {
                        setCommentSelect({...item, comments: []});
                        setShowModal(true);
                      }
                    }}>
                    <Text style={styles.nameAuthor}>{item.author.name}</Text>
                    <Text style={styles.textComment}>{item.text}</Text>
                  </Pressable>
                </View>
              </View>
            </View>
          );
        })}
      <ModalActionComment
        visible={showModal}
        onClose={() => setShowModal(false)}
        comment={commentSelect}
        commentsData={props.commentsData}
        setCommentsData={props.setCommentsData}
      />
    </View>
  );
};

export default ItemComment;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: appSize(10),
  },
  avatar: {
    width: appSize(40),
    height: appSize(40),
    borderRadius: appSize(40),
    marginRight: appSize(10),
  },
  avatarSub: {
    width: appSize(30),
    height: appSize(30),
    borderRadius: appSize(30),
    marginRight: appSize(8),
  },
  nameAuthor: {
    fontWeight: 'bold',
    fontSize: appSize(15),
    marginBottom: appSize(5),
    color: color.black,
  },
  viewComment: {
    backgroundColor: color.black006,
    padding: appSize(10),
    borderRadius: appSize(10),
  },
  textReply: {
    fontSize: appSize(12),
    fontWeight: 'bold',
  },
  row: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
  },
  textComment: {
    color: color.black,
    fontSize: appSize(13),
  },
});
